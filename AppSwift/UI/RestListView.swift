//
//  RestListView.swift
//  AppSwift
//
//  Created by Developer ZoeWave on 2/6/20.
//  Copyright © 2020 Developer YLabz. All rights reserved.
//

import SwiftUI

let first = Restaurant(name: "Joe's Original")
let second = Restaurant(name: "The Real Joe's Original")
let third = Restaurant(name: "Original Joe's")

struct Restaurant: Identifiable {
    var id = UUID()
    var name: String
}

struct RestaurantRow: View {
    var restaurant: Restaurant
    var body: some View {
        Text("Come and eat at \(restaurant.name)")
    }
}

struct DetailView: View {
    let discipline: String
    var body: some View {
        Text(discipline)
    }
}

struct RestListView: View{
    @State private var restaurants = [first, second, third]
    
    
    
    var body: some View {
        NavigationView {
            List {
                ForEach(restaurants) { rest in
                    NavigationLink(destination: DetailView(discipline: rest.name)) {
                        HStack {
                            Image("Ash").resizable().frame(width: 40, height: 40)
                            Text("\(rest.name)")
                        }
                        
                    }
                    .foregroundColor(Color.green)
                }
                .onDelete(perform: delete)
                .onMove(perform: move)
            }
            .navigationBarTitle("Eat Here")
            .navigationBarItems(
                leading: Button(action: {
                    self.addRow()
                }) {
                    Image(systemName: "plus")
                },
                trailing: EditButton())
        }
    }
    
    private func addRow() {
        let rest = Restaurant(name: "Joe's Not Original \(Int.random(in: 0..<500))")
        self.restaurants.append(rest)
        
    }
    
    func delete(at offsets: IndexSet) {
        restaurants.remove(atOffsets: offsets)
    }
    
    func move(from source: IndexSet, to destination: Int) {
        restaurants.move(fromOffsets: source, toOffset: destination)
    }
    
}//return List(restaurants, rowContent: RestaurantRow.init)


struct RestListView_Previews: PreviewProvider {
    static var previews: some View {
        RestListView()
    }
}
