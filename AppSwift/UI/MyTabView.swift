//
//  MyTabView.swift
//  AppSwift
//
//  Created by Developer ZoeWave on 2/6/20.
//  Copyright © 2020 Developer YLabz. All rights reserved.
//

import SwiftUI

struct MyTabView: View {
    var body: some View {
        TabView {
            RestListView()
                //Text("The First Tab")
                .tabItem {
                    Image(systemName: "1.square.fill")
                    Text("First")
            } //Text("Third")
            ContentView()
                //Text("The Second Tab")
                .tabItem {
                    Image(systemName: "2.square.fill")
                    Text("Second")
            }
            VStack {
                Text("The Last Tab")
                Button(action:
                { TryCombine().callNum() }) {
                    HStack {
                        Image(systemName: "plus.circle")
                        Text("Add 1")
                    }
                }
            }  .tabItem {
                Image(systemName: "3.square.fill")
                Text("Third")
            }
        }
            .font(.headline)
        }
    }


struct MyTabView_Previews: PreviewProvider {
    static var previews: some View {
        MyTabView()
    }
}
