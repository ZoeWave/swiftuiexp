//
//  TestUIView.swift
//  AppSwift
//
//  Created by Developer ZoeWave on 2/8/20.
//  Copyright © 2020 Developer YLabz. All rights reserved.
//

import SwiftUI

struct TestUIView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TestUIView_Previews: PreviewProvider {
    static var previews: some View {
        TestUIView()
    }
}
