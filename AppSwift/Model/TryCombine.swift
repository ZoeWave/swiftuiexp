//
//  TryCombine.swift
//  AppSwift
//
//  Created by Developer ZoeWave on 2/8/20.
//  Copyright © 2020 Developer YLabz. All rights reserved.
//

import Foundation
import Combine

struct TryCombine {
    
    let numbers = [1,2,3,4,5,7,8,9].publisher
    
    func callNum() {
        call(num: numbers)
    }
    
    
    private func call(num : Publishers.Sequence<[Int], Never>) {
        let ans = num.map {
            value -> String in
            switch value {
            case _ where value < 1:
                return "none"
            case _ where value == 1:
                return "one"
            case _ where value == 2:
                return "couple"
            case _ where value == 3:
                return "few"
            case _ where value > 8:
                return "many"
            default:
                return "some"
            }
        }
        print(ans)
    }
    

    func callNum(num : Int) {
        let _ = Just(num)
            .map { value -> String in
                switch value {
                case _ where value < 1:
                    return "none"
                case _ where value == 1:
                    return "one"
                case _ where value == 2:
                    return "couple"
                case _ where value == 3:
                    return "few"
                case _ where value > 8:
                    return "many"
                default:
                    return "some"
                }
        }
        .sink { receivedValue in
            print("The end result was \(receivedValue)")
        }
    }
}
